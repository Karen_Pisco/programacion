class Region:
    def __init__(self,nombre,numero_provincia,clima):
        self.nombre=nombre
        self.numero_provincia=numero_provincia
        self.clima = clima
    def obtenerNombre(self):
        return self.nombre
    def obtenerNumeroProvincia(self):
        return self.numero_provincia
    def obtenerClima(self):
        return self.clima
    def obtenerInformacion(self):
        print( " La region "+str(self.nombre)+" esta compuesta por "+str(self.numero_provincia)+" provicias y tiene un clima "+str(self.clima)+". ")