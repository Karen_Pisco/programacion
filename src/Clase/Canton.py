class Canton:
    def __init__(self,nombre,numero_habitantes,latitud,longitud):
        self.nombre=nombre
        self.numero_habitantes=numero_habitantes
        self.latitud=latitud
        self.longitud=longitud
    def obtenerNombre(self):
        return self.nombre
    def obtenerNumeroHabitantes(self):
        return self.numero_habitantes
    def obtenerLatitud(self):
        return self.latitud
    def obtenerLongitud(self):
        return self.longitud
    def obtenerInformacion(self):
        print("El canton "+str(self.nombre)+" esta ubicado en la posicion geografica "+str(self.latitud)+" "+str(self.longitud)+", posee una poblacion de "+str(self.numero_habitantes)+" habitantes")