from HerenciaSimple.Animal import Animal

class Ave(Animal):

    def __init__(self,tipo_alimentacion,tipo,numero_patas,peso):
        super().__init__(tipo,numero_patas,peso)
        self.tipo_alimentacion=tipo_alimentacion
        
    def obtenerTipo_alimentacion(self):
        return self.tipo_alimentacion
    
    def obtenerInformacion(self):
        return ( super().obtenerInformacion()+ "El ave es de tipo de alimentacion "+str(self.tipo_alimentacion))

    