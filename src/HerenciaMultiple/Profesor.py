
from HerenciaMultiple.Empleado import Empleado
from HerenciaMultiple.Persona import Persona


class Profesor(Persona,Empleado):

    def __init__(sefl,codigo, materia, fecha_ingreso,dni,nombres,apellidos,sueldo,profesion):
        Persona.__init__(sefl,dni,nombres,apellidos)
        Empleado.__init__(sefl,sueldo,profesion)
        sefl.codigo=codigo
        sefl.materia=materia
        sefl.fecha_ingreso=fecha_ingreso
    
    def obtenerCodigo(sefl):
        return sefl.codigo
    
    def obtenerMateria(sefl):
        return sefl.materia

    def obtenerFechaIngreso(sefl):
        return sefl.fecha_ingreso

    def obtenerInformacion(sefl):
        return ( Persona.obtenerInformacion(sefl)+". "+Empleado.obtenerInformacion(sefl) + ". El codigo del docente es "+str(sefl.codigo)+" da la materia de "+str(sefl.materia)+" ingreso a la instucion en la fecha "+str(sefl.fecha_ingreso))
        