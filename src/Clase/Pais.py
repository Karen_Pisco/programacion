class Pais:
    def __init__(self,codigo,nombre,zona_horaria,icono_bandera):
        self.codigo=codigo
        self.nombre=nombre
        self.zona_horaria=zona_horaria
        self.icono_bandera=icono_bandera
    def obtenerCodigo(self):
        return self.codigo
    def obtenerNombre(self):
        return self.nombre
    def obtenerZonaHoraria(self):
        return self.zona_horaria
    def obtenerIconoBandera(self):
        return self.icono_bandera
    def obtenerInformacion(self):
        print("El pais de "+str(self.nombre)+" hoy se enfrenta a la poderosa seleccion de brazil, la seleccion de "+str(self.nombre)+ " esta ubicada en la zona horaria "+str(self.zona_horaria)+", "+str(self.icono_bandera)+" vamos ecuador hoy se hace historia.")