

from HerenciaSimple.Ave import Ave


class Gallo(Ave):

    def __init__(self,crianza,tipo_alimentacion,tipo,numero_patas,peso):
        super().__init__(tipo_alimentacion,tipo,numero_patas,peso)
        self.crianza=crianza
    
    def obtenerCrianza(self):
        return self.crianza
    
    def obtenerInformacion(self):
        return super().obtenerInformacion()+", esta ave es de craianza de "+str(self.crianza)


        