

from HerenciaMultiple.Empleado import Empleado
from HerenciaMultiple.Persona import Persona


class Vicedecano(Persona,Empleado):

    def __init__(sefl,horario,dni,nombres,apellidos,sueldo,profesion):
        Persona.__init__(sefl,dni,nombres,apellidos)
        Empleado.__init__(sefl,sueldo,profesion)
        sefl.horario=horario
    
    def obtenerHorario(sefl):
        return sefl.horario

    def obtenerInformacion(sefl):
        return (Persona.obtenerInformacion(sefl)+". "+Empleado.obtenerInformacion(sefl) + "El sueldo del decano es "+str(sefl.sueldo)+" y esta disponible los dias "+str(sefl.horario))
        