from Clase.Canton import Canton
from Clase.Pais import Pais
from Clase.Presidente import Presidente
from Clase.Provincia import Provincia
from Clase.Region import Region
from HerenciaMultiple.Decano import Decano
from HerenciaMultiple.Investigador import Investigador
from HerenciaMultiple.PresidenteProfesores import PresidenteProfesores
from HerenciaMultiple.Profesor import Profesor
from HerenciaMultiple.Vicedecano import Vicedecano
from HerenciaSimple.Ave import Ave
from HerenciaSimple.Gallo import Gallo
from HerenciaSimple.Gato import Gato
from HerenciaSimple.Perro import Perro
from HerenciaSimple.French import French

print("***********************  5 EJERCICIOS DE CLASES  *********************** \n")

print("1.-) Clase Pais")
pais = Pais("593","Ecuador","UTC-5","bandera_ecuador.png")
pais.obtenerInformacion()

print("\n")

print("2.-) Clase Provincia")
provincia = Provincia("Manabi","1 395 249","18 940",22)
provincia.obtenerInformacion()

print("\n")

print("3.-) Clase Canton")
canton = Canton("Portoviejo",",238 430","-1.05458","-80.45445")
canton.obtenerInformacion()

print("\n")

print("4.-) Clase Region")
region = Region("Costa",24,"caluroso")
region.obtenerInformacion()

print("\n")

print("5.-) Presidente")
presidente = Presidente("Karen Thalia","Pisco Ibarra","Ecuatoriana","1999/07/18")
presidente.obtenerInformacion()


print("\n")
print("\n")

print("***********************  5 EJERCICIOS DE HERENCA SIMPLE y OVERRIDING  *********************** \n")

print("1.-) Clase Perro")

perro = Perro("Bley","cafe","malasia","terrestre",4,"10kg")
print(perro.obtenerInformacion())


print("\n")

print("2.-) Clase French")
french = French("5 años","Gio","cafe","malasia","terrestre",4,"12kg")
french.obtenerInformacion()


print("\n")

print("3.-) Clase Ave")
ave = Ave("omnívoro","volador","2","5kg")
print(ave.obtenerInformacion())

print("\n")

print("4.-) Clase Gallo")
gallo = Gallo("granja","hervivoro","terrestre","2","4kg")
print(gallo.obtenerInformacion())

print("\n")

print("5.-) Clase Gato")
gato = Gato("blanco","terrestre",4,"5kg")
print(gato.obtenerInformacion())

print("\n")



print("\n")
print("\n")

print("***********************  5 EJERCICIOS DE HERENCA MULTIPLE  *********************** \n")

print("1.-) Clase Profesor")
profesor = Profesor("P001","Algoritmo","01/01/2022","1315915460","jean carlos", "piguave alvarado",500, "ing sistemas")
print(profesor.obtenerInformacion())

print("\n")

print("2.-) Clase Investigador")
investigaor = Investigador("33","packman","kpisco@hotmail.com","1315915460","jean carlos", "piguave alvarado",500, "ing sistemas")
print(investigaor.obtenerInformacion())

print("\n")

print("3.-) Clase PresidenteProfesores")
presidente_profesores = PresidenteProfesores("Lunea,Miercoles,Viernes","1315915460","jean carlos", "piguave alvarado",500, "ing sistemas")
print(presidente_profesores.obtenerInformacion())

print("\n")

print("4.-) Clase Gato")
decano = Decano("8AM:16PM","1315915460","jean carlos", "piguave alvarado",500, "ing sistemas")
print(decano.obtenerInformacion())

print("\n")

print("5.-) Clase Gato")
vicedecano = Vicedecano("8AM:16PM","1315915460","jean carlos", "piguave alvarado",500, "ing sistemas")
print(vicedecano.obtenerInformacion())