class Provincia:
    def __init__(self,nombre,numero_habitantes,dimension,cantones):
        self.nombre=nombre
        self.numero_habitantes=numero_habitantes
        self.dimension=dimension
        self.cantones=cantones
    def obtenerNombre(self):
        return self.nombre
    def obtenerNumeroHabitantes(self):
        return self.numero_habitantes
    def obtenerDimension(self):
        return self.dimension
    def obtenerCantones(self):
        return self.cantones
    def obtenerInformacion(self):
        print("La provincia de "+str(self.nombre)+" tiene "+str(self.numero_habitantes)+" de habitantes y cuenta con una dimension de "+str(self.dimension)+" km^2, con "+str(self.cantones)+" cantones. ")