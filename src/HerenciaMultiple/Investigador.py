
from HerenciaMultiple.Empleado import Empleado
from HerenciaMultiple.Persona import Persona
from HerenciaMultiple.Profesor import Profesor


class Investigador(Persona,Empleado):

    def __init__(sefl,libros_escritos, editoriales,correo,dni,nombres,apellidos,sueldo,profesion):
        Persona.__init__(sefl,dni,nombres,apellidos)
        Empleado.__init__(sefl,sueldo,profesion)
        sefl.libros_escritos=libros_escritos
        sefl.editoriales=editoriales
        sefl.correo=correo
    
    def obtenerLibrosEscritos(sefl):
        return sefl.libros_escritos
    
    def obtenerEditoriales(sefl):
        return sefl.editoriales

    def obtenerCorreo(sefl):
        return sefl.correo

    def obtenerInformacion(sefl):
        return (Persona.obtenerInformacion(sefl)+". "+Empleado.obtenerInformacion(sefl) + "El numero de libros escritor por el investigador es "+str(sefl.libros_escritos)+" en la editorial "+str(sefl.editoriales)+" su correo es: "+str(sefl.correo))
        