
from HerenciaMultiple.Empleado import Empleado
from HerenciaMultiple.Persona import Persona


class PresidenteProfesores(Persona,Empleado):

    def __init__(sefl,horario,dni,nombres,apellidos,sueldo,profesion):
        Persona.__init__(sefl,dni,nombres,apellidos)
        Empleado.__init__(sefl,sueldo,profesion)
        sefl.sueldo=sueldo
        sefl.horario=horario
    
    def obtenerHorario(sefl):
        return sefl.horario

    def obtenerInformacion(sefl):
        return (Persona.obtenerInformacion(sefl)+". "+Empleado.obtenerInformacion(sefl) +" y el presidente  esta disponible los dias "+str(sefl.horario))
        