class Animal:
    def __init__(self,tipo,numero_patas,peso):
        self.tipo=tipo
        self.numero_patas=numero_patas
        self.peso= peso

    def obtenerTipo(self):
        return self.tipo

    def obtenerNumeroPatas(self):
        return self.numero_patas
        
    def obtenerPeso(self):
        return self.peso

    def obtenerInformacion(self):
        return (" El animal es de tipo "+str(self.tipo)+ " el numero de patas es "+str(self.numero_patas)+" tiene un peso de"+str(self.peso))
    
