
from HerenciaSimple.Animal import Animal

class Perro(Animal):
    def __init__(self,nombre,color,origen,tipo,numero_patas,peso):
        super().__init__(tipo,numero_patas,peso)
        self.color= color
        self.origen=origen
        self.nombre=nombre
    
    def obtenerColor(self):
        return self.color
        
    def obtenerOrigen(self):
        return self.origen
    
    def obtenerNombre(self):
        return self.nombre
        
    def obtenerInformacion(self):
        return super().obtenerInformacion()+",el nombre del perro es "+str(self.nombre)+" tiene un color de "+str(self.color)+" y tiene un origen en "+str(self.origen)