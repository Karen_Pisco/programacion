from HerenciaSimple.Animal import Animal

class Gato(Animal):

    def __init__(self, color, tipo, numero_patas, peso):
        super().__init__(tipo, numero_patas, peso)
        self.color=color
    
    def obtenerColor(self):
        return self.color

    def obtenerInformacion(self):
        return super().obtenerInformacion()+" tiene un color "+str(self.color)