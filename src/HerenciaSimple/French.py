
from HerenciaSimple.Perro import Perro


class French(Perro):

    def __init__(self,nombre,edad,color,origen,tipo,numero_patas,peso):
        super().__init__(nombre,color,origen,tipo,numero_patas,peso)
        self.edad=edad

    def obtenerEdad(self):
        return self.edad 
    
    def obtenerInformacion(self):
        print(super().obtenerInformacion()+" con una edad de"+(self.edad)) 

        